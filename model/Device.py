from cqlengine import columns
from cqlengine.models import Model

class Device(Model):
    __table_name__ = "device"
    __keyspace__ = "checkimei"
    device_id = columns.Text(primary_key=True)
    imei = columns.Text(index=True)
    user_id = columns.UUID(index=True)
    apelido = columns.Text()
    marca = columns.Text()
    modelo = columns.Text()
    data_insercao = columns.DateTime(primary_key=True)

