from cqlengine import columns
from cqlengine.models import Model


class Certificado_Device(Model):
    __table_name__ = "certificado_device"
    __keyspace__ = "checkimei"
    device_id = columns.Text(primary_key=True)
    local_certificado = columns.Text()
    data_criacao = columns.DateTime(primary_key=True)
