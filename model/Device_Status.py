from cqlengine import columns
from cqlengine.models import Model

class Device_Status(Model):
    __table_name__ = "device_status"
    __keyspace__ = "checkimei"
    device_id = columns.Text(primary_key=True)
    imei = columns.Text()
    status = columns.Text()
    info_status = columns.Map(columns.Text,columns.Text)
    data_insercao = columns.DateTime(primary_key=True)
