from cqlengine import columns
from cqlengine.models import Model
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)


class User(Model):
    __table_name__ = "user"
    __keyspace__ = "checkimei"
    user_id = columns.UUID(primary_key=True)
    email = columns.Text(index=True)
    password = columns.Text()
    name = columns.Text()
    surname = columns.Text()
    birth_date = columns.DateTime()
    gender = columns.Text()
    created_at = columns.DateTime()


    def __repr__(self):
        return '%s %s' % (self.name, self.gender)

    def hash_password(self, password):
        self.password = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)

    def generate_auth_token(self, expiration=600):
        s = Serializer("nao tem uma secret key ainda", expires_in=expiration)
        return s.dumps({'id': self.email})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer("nao tem uma secret key ainda")
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.objects.filter(email=data['id']).first()
        return user
