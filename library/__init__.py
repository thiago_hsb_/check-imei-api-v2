# -*- coding: utf-8 -*-
"""
This module provides facilities for params, log, db connection, api calls, etc.
"""

import os
import sys
import json
import time
import yaml
import logging
from collections import Mapping
from argparse import ArgumentParser

# JSON error handler
json_decoder_errmsg_original = json.decoder.errmsg


class GetArgs(ArgumentParser):
    """
    Fast setup command line argument

    Eg.:
    ARGS_DEF = {
        'description': 'Crawler de Importacao Oracle',
    }
    ARGS_LIST = [
        [
            [
                '-c',
                '--config_file'
            ],
            {
                'required': True,
                'help': 'nome do arquivo de configuracao'
            }
        ],
        [
            '--mock',
            {
                'required': False,
                'help': 'Indica se esta rodando no modo mock'
            }
        ]
    ]
    args = GetArgs(ARGS_DEF, ARGS_LIST)
    args.parse_args()

    """

    def __init__(self, attrs=None, args=[]):
        ArgumentParser.__init__(self, **attrs)
        for arg in args:
            if type(arg[0]) == list:
                self.add_argument(*arg[0], **arg[1])
            elif type(arg[0]) == str:
                self.add_argument(arg[0], **arg[1])


class Log(logging.Logger):
    """
    Facility for writing log files
    This class creates a logging.Logger instance and attach a StreamHandler to it, writing to a predefined stream

    """

    # Log levels
    CRITICAL = logging.CRITICAL
    FATAL = logging.FATAL
    ERROR = logging.ERROR
    WARNING = logging.WARNING
    WARN = logging.WARN
    INFO = logging.INFO
    DEBUG = logging.DEBUG
    NOTSET = logging.NOTSET

    _handler = None
    _old_level = None

    def __init__(
            self,
            stream=sys.stderr,
            level=INFO,
            fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            name=__file__
    ):
        """
        Log constructor

        :param stream: Data stream to write to
        :param level: Log level (same as logging module)
        :param fmt: Log format
        """

        logging.Logger.__init__(self, name, level=level)

        # Open stream for writing
        if type(stream) is str or type(stream) is unicode:
            # Create a log file
            self._handler = logging.StreamHandler(
                open(stream, mode='a')
            )
        else:
            # Log to 'stream'
            self._handler = logging.StreamHandler(stream)

        # Store level for use when leave debug mode
        self._old_level = level

        # Set formatter
        formatter = logging.Formatter(fmt)
        formatter.converter = time.gmtime
        self._handler.setFormatter(
            formatter
        )

        # Set log handler
        self.addHandler(self._handler)

    def set_debug(self, status=True):
        """
        Switch debug mode on/off

        :param status: Debug mode (boolean)
        """

        if status:
            self.setLevel(self.DEBUG)
        else:
            self.setLevel(self._old_level)
    #
    # def info(self, msg, *args, **kwargs):
    #     return super(Log, self).info(msg, *args, **kwargs)


def load_json(stream):
    """
    Convert the given JSON stream to object
    Threat the input stream to avoid JSON errors

    :param stream: File handler or stream with JSON
    :return: Object
    """

    output = None

    # Remove bad JSON chars
    if type(stream) is file:
        output = ''.join(l.replace('\r', ' ').replace('\n', ' ').replace('\t', ' ') for l in stream)
    else:
        output = str(stream).replace('\r', ' ').replace('\n', ' ').replace('\t', ' ')

    try:
        data = json.loads(output)
        return data

    except ValueError as e:
        # Could not parse data to JSON
        raise e

    except Exception as e:
        msg = "Error parsing JSON data! %s %s" % (stream, str(e))
        raise Exception(msg)


def init_log():
    """
    Initialize log tool

    :return: Log tool (object)
    """

    # Generate logfile into the script's directory
    log_file = "%s/logs/checkimei.log" % os.path.dirname(os.path.abspath(sys.argv[0]))

    try:
        log = Log(name="checkimei", stream=log_file)

    except Exception as e:
        print >> sys.stderr, 'Error creating logging object manipulator! %s' % str(e)
        return False

    return log


def init_args():
    """
    Get command line arguments

    :return: Arguments (list)
    """

    args_def = {
        'description': 'API para os fornecedores de preços de mercado dos produtos',
    }
    args_list = [
        [
            [
                '-d',
                '--debug'
            ],
            {
                'required': False,
                'dest': 'debug',
                'action': 'store_true',
                'help': 'Modo debug'
            }
        ],
        [
            '--mock',
            {
                'required': False,
                'dest': 'mock',
                'action': 'store_true',
                'help': 'Modo teste'
            }
        ]
    ]

    args_handler = GetArgs(args_def, args_list)
    return args_handler.parse_args()


def init_conf(log=None):
    """
    Read the crawler configuration files

    :param log: Logging object
    :return: Project configuration (object)
    """

    conf = None
    try:
        # Load conf
        conf_file = "%s/conf/config.yml" % os.path.dirname(os.path.abspath(sys.argv[0]))
        conf_obj = yaml.load(open(conf_file, 'r'))
        conf = conf_obj

    except Exception as e:
        msg = "Error loading config file! %s" % str(e)
        if log:
            log.error(msg)
        raise Exception(msg)

    return conf


def merge(data1, data2, depth=-1):
    """
    Recursively merge or update dict-like objects.
    >>> merge({'k1': {'k2': 2}}, {'k1': {'k2': {'k3': 3}}, 'k4': 4})
    {'k1': {'k2': {'k3': 3}}, 'k4': 4}

    :param data1: First object to merge
    :param data2: Second object to merge
    :return: data1 and data2 merged (complex object)
    """

    for k, v in data2.iteritems():
        if isinstance(v, Mapping) and not depth == 0:
            r = merge(data1.get(k, {}), v, depth=max(depth - 1, -1))
            data1[k] = r
        elif isinstance(data1, Mapping):
            data1[k] = data2[k]
        else:
            data1 = {k: data2[k]}
    return data1


def json_handle_error(msg, doc, pos, end=None):
    json.last_error_position = json.decoder.linecol(doc, pos)

    msg = """Bad JSON data near -->%s""" % doc[pos-20:pos+20]
    log = init_log()
    log.error(msg)
    return json_decoder_errmsg_original(msg, doc, pos, end)


# Module init
if __name__ == "library":
    # JSON hook facility to debug bat JSON data
    json.decoder.errmsg = json_handle_error
