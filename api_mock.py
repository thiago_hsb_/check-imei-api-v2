#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Thiago Henrique'

import json
from flask import Flask, request, abort, jsonify, g
from datetime import datetime
from flask.ext.httpauth import HTTPBasicAuth
import uuid
import library

app = Flask("check-imei-api")
app.config['SECRET_KEY'] = 'Se aqui nevasse eu usava esqui'
log = library.init_log()
app._logger = log
args = library.init_args()
auth = HTTPBasicAuth()


@app.route('/api/status', methods=['GET'])
def status():
    """
    Returns to client the status of this API
    """
    return jsonify(status='ok')


@app.route('/api/check_email_exists', methods=['POST'])
def check_email_exists():
    """
    Check in database if the username exists.
    Ex.: {"email": "fulano@gmail.com"}
    """
    if not request.json:
        log.error("JSON invalido")
        abort(400)
    try:
        if request.json[u"email"].encode('utf-8') == "fulano@gmail.com":
            return jsonify(email_exists=True)
        else:
            return jsonify(email_exists=False)
    except Exception, ex:
        log.error("Erro ao consultar email: " + str(ex.message))
        log.error(str(request.json))
        abort(500)


@app.route('/api/users', methods=['POST'])
def new_user():
    """
    Create new user.
    Ex.:
    {
        "email": "fulano@gmail.com",
        "password": "fulano123",
        "name": "Fulano",
        "surname": "de Tal",
        "gender": "masculino",
        "birth_date": "03-10-1993"
    }
    """
    if not request.json:
        log.error("JSON invalido")
        abort(400)

    try:
        return jsonify(username=email, created=True)
    except Exception, ex:
        log.error(str(ex))
        abort(500)


@app.route('/api/login', methods=['POST'])
@auth.login_required
def login():
    return jsonify(authenticate=True)

@auth.verify_password
def verify_password(email_or_token, password):
    try:
        if email_or_token == "123456789":
            return True
        elif email_or_token == "fulano@gmail.com" and password == "fulano123":
            return True
        else:
            return False
    except Exception, ex:
        log.error(str(ex))

@app.route('/api/token')
@auth.login_required
def get_auth_token():
    return jsonify({'token': '123456789', 'duration': 600})


@app.route('/api/cadastro_imei', methods=['POST'])
@auth.login_required
def cadastroImei():
    """
    Create new IMEI.
    Ex.:
    {
        "IMEI": "1234567890123",
        "apelido": "Galaxy S5",
        "marca": "Samsung",
        "modelo": "Galaxy S5 GT9000"
    }
    """
    try:
        return jsonify(imei_cadastrado=True)

    except Exception, ex:
        log.error(ex.message)
        abort(500)

@app.route('/api/update_status', methods=['POST'])
@auth.login_required
def update_status_imei():
    """
    Update Status IMEI.
    Ex.:
    {
        "IMEI": "1234567890123",
        "status": "Roubado",
        "infos": {"bo": "12345", "distrito": "23","cidade_delegacia": "Santos", "estado_delegacia": "SP", "data_roubo": "10-10-2015", "local_roubo": "Rua xpto, 100, Santos - SP"}
    }
    """
    try:
        imei = request.json[u'IMEI'].encode('utf-8')
        status = request.json[u'status'].encode('utf-8')
        infos = request.json[u'infos']

        return jsonify(status_alterado=True)

    except Exception, ex:
        log.error(ex.message)
        abort(500)

@app.route('/api/check_imei', methods=['POST'])
def check_imei():
    """
    Check Status IMEI.
    Ex.:
    {
        "IMEI": "1234567890123"
    }
    """

    try:
        imei=request.json[u'IMEI'].encode('utf-8')

        if imei == "123465":
            return jsonify(imei=imei, status="Roubado", infos={"bo": "12345", "distrito": "23","cidade_delegacia": "Santos", "estado_delegacia": "SP", "data_roubo": "10-10-2015", "local_roubo": "Rua xpto, 100, Santos - SP"})
        elif imei == "54321":
            return jsonify(imei=imei, status="Furtado", infos={"bo": "12345", "distrito": "23","cidade_delegacia": "Santos", "estado_delegacia": "SP", "data_roubo": "10-10-2015", "local_roubo": "Rua xpto, 100, Santos - SP"})
        else:
            return jsonify(imei=imei, status="OK")

    except Exception, ex:
        log.error(ex.message)
        abort(500)


if __name__ == '__main__':
    log.debug(args.debug)

    log.info("Starting API")

    app.run(host="0.0.0.0", port=80, debug=args.debug)
