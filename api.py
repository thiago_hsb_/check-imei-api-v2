#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Thiago Henrique'

import json
from flask import Flask, request, abort, jsonify, g
from flask.ext.autodoc import Autodoc
from cqlengine import connection
from datetime import datetime
from model.User import User
from model.Device import Device
from model.Device_Status import Device_Status
from model.Certificado_Device import Certificado_Device
from flask.ext.httpauth import HTTPBasicAuth
import uuid
import library

app = Flask("check-imei-api")
app.config['SECRET_KEY'] = 'Se aqui nevasse eu usava esqui'
auto = Autodoc(app)
log = library.init_log()
app._logger = log
args = library.init_args()
auth = HTTPBasicAuth()

@app.route('/api/status', methods=['GET'])
@auto.doc()
def status():
    """
    Returns to client the status of this API
    """
    return jsonify(status='ok')


@app.route('/api/check_email_exists', methods=['POST'])
@auto.doc()
def check_email_exists():
    """
    Check in database if the username exists.
    Ex.: {"email": "fulano@gmail.com"}
    """
    if not request.json:
        log.error("JSON invalido")
        abort(400)
    try:
        if User.objects.filter(email=request.json[u"email"].encode('utf-8')).first() is None:
            log.info(request.json)
            return jsonify(email_exists=False)
        else:
            return jsonify(email_exists=True)
    except Exception, ex:
        log.error("Erro ao consultar email: " + str(ex.message))
        log.error(str(request.json))
        abort(500)


@app.route('/api/users', methods=['POST'])
@auto.doc()
def new_user():
    """
    Create new user.
    Ex.:
    {
        "email": "fulano@gmail.com",
        "password": "fulano123",
        "name": "Fulano",
        "surname": "de Tal",
        "gender": "masculino",
        "birth_date": "03-10-1993"
    }

    curl -H "Content-Type: application/json" -X POST -d '{ "email": "fulano@gmail.com", "password": "fulano123", "name": "Fulano", "surname": "de Tal", "gender": "masculino", "birth_date": "03-10-1993"}' http://localhost:80/api/users
    """
    if not request.json:
        log.error("JSON invalido")
        abort(400)

    user_id = uuid.uuid4()
    email = request.json[u'email'].encode('utf-8')
    password = request.json[u'password'].encode('utf-8')
    name = request.json[u'name'].encode('utf-8')
    surname = request.json[u'surname'].encode('utf-8')
    gender = request.json[u'gender'].encode('utf-8')
    birth_date = None
    created_at = datetime.now()

    try:
        birth_date = datetime.strptime(request.json[u'birth_date'].encode('utf-8'), "%d-%m-%Y")
    except ValueError, ex:
        abort(400)

    try:
        if email is None or password is None or name is None or surname is None or gender is None or birth_date is None:
            abort(400)  # missing arguments

        if User.objects.filter(email=email).first():
            abort(400)  # existing email

        print user_id, password, name, surname, gender

        user = User(user_id=user_id, email=email, name=name, surname=surname, gender=gender, birth_date=birth_date, created_at=created_at)
        user.hash_password(password)
        user.save()
        return jsonify(username=user.email, created=True)
    except Exception, ex:
        log.error(str(ex))
        abort(500)


@app.route('/api/login', methods=['POST'])
@auto.doc()
@auth.login_required
def login():
    return jsonify(authenticate=True)

@auth.verify_password
def verify_password(email_or_token, password):
    try:
        user = User.verify_auth_token(email_or_token)
        if not user:
            user = User.objects.filter(email=email_or_token).first()
            if not user.verify_password(password):
                return False
        g.user = user
        return True
    except Exception, ex:
        log.error(str(ex))


@app.route('/api/token')
@auto.doc()
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})


@app.route('/api/cadastro_device', methods=['POST'])
@auto.doc()
@auth.login_required
def cadastro_device():
    """
    Create new IMEI.
    Ex.:
    {
        "device_id": "1a2b3c4d",
        "imei": "1234567890123",
        "apelido": "Galaxy S5",
        "marca": "Samsung",
        "modelo": "Galaxy S5 GT9000"
    }

    curl -H "Content-Type: application/json" -X POST -d '{"device_id": "1a2b3c4d", "imei": "1234567890123",  "apelido": "Galaxy S5", "marca": "Samsung", "modelo": "Galaxy S5 GT9000"}' http://localhost:80/api/cadastro_devices -u eyJhbGciOiJIUzI1NiIsImV4cCI6MTQ0MzA1MTA0MSwiaWF0IjoxNDQzMDUwNDQxfQ.eyJpZCI6ImZ1bGFub0BnbWFpbC5jb20ifQ.cY-fsQSfkzDuR96CQu-mesWMvZAigl2iRs6gtABcYP0:

    """
    try:
        imei = request.json[u'imei'].encode('utf-8')
        device_id = request.json[u'device_id'].encode('utf-8')

        if Device.objects.filter(device_id=device_id).first():
            abort(400)

        apelido = request.json[u'apelido'].encode('utf-8')
        marca = request.json[u'marca'].encode('utf-8')
        modelo = request.json[u'modelo'].encode('utf-8')
        user_id = g.user.user_id
        date = datetime.now()

        new_device = Device(device_id=device_id, imei=imei, user_id=user_id, apelido=apelido, marca=marca, modelo=modelo, data_insercao=date)
        new_device.save()

        log.info("device inserido")

        imei_status = Device_Status(device_id=device_id, info_status=None, status="OK", data_insercao=date)
        imei_status.save()

        log.info("Status do Device alterado")

        return jsonify(imei_cadastrado=True)

    except Exception, ex:
        log.error(ex)
        abort(500)

@app.route('/api/update_status', methods=['POST'])
@auto.doc()
@auth.login_required
def update_status_device():
    """
    Update Status Device.
    Ex.:
    {
        "device_id": "1a2b3c4d",
        "status": "Roubado",
        "infos": {"bo": "12345", "distrito": "23","cidade_delegacia": "Santos", "estado_delegacia": "SP", "data_roubo": "10-10-2015", "local_roubo": "Rua xpto, 100, Santos - SP"}
    }
    """
    try:
        device_id = request.json[u'device_id'].encode('utf-8')
        status = request.json[u'status'].encode('utf-8')
        infos = request.json[u'infos']

        if Device.objects.filter(device_id=device_id).first().user_id != g.user.user_id:
            abort(400)

        #verify if device exists
        if Device_Status(device_id=device_id).filter().first():
            device_status = Device_Status(device_id=device_id, status=status, info_status=infos, data_insercao=datetime.now())
            device_status.save()
        else:
            abort(400)

        return jsonify(status_alterado=True)

    except Exception, ex:
        log.error(ex.message)
        abort(500)

@app.route('/api/update_device', methods=['POST'])
@auto.doc()
@auth.login_required
def update_device():
    """
    Update Status Device.
    Ex.:
    {
        "device_id": "1a2b3c4d",
        "imei": "1234567890123",
        "apelido": "Galaxy S5",
        "marca": "Samsung",
        "modelo": "Galaxy S5 GT9000"
        "status": "Roubado",
        "infos": {"bo": "12345", "distrito": "23","cidade_delegacia": "Santos", "estado_delegacia": "SP", "data_roubo": "10-10-2015", "local_roubo": "Rua xpto, 100, Santos - SP"}
    }
    """
    try:
        device_id = request.json[u'device_id'].encode('utf-8')
        apelido = request.json[u'apelido'].encode('utf-8')
        marca = request.json[u'marca'].encode('utf-8')
        modelo = request.json[u'modelo'].encode('utf-8')
        imei = request.json[u'imei'].encode('utf-8')
        user_id = g.user.user_id
        date = datetime.now()
        status = request.json[u'status'].encode('utf-8')
        infos = request.json[u'infos'].encode('utf-8')

        if Device.objects.filter(device_id=device_id).first().user_id != g.user.user_id:
            abort(400)

        #verify if device exists
        if Device(device_id=device_id).filter().first():
            device = Device(device_id=device_id).filter().first()
            device.imei = imei
            device.apelido = apelido
            device.marca = marca
            device.modelo = modelo
            device.save()
        else:
            abort(400)

        device_status = Device_Status(device_id=device_id, status=status, info_status=None, data_insercao=datetime.now())
        device_status.save()


        return jsonify(device_alterado=True)

    except Exception, ex:
        log.error(ex.message)
        abort(500)

@app.route('/api/update_password', methods=['POST'])
@auto.doc()
@auth.login_required
def update_password():
    """
    Check Status IMEI.
    Ex.:
    {
        "password": "1234567890123",
        "new_password": "a1b2c3",
        "confirm_password": "a1b2c3"
    }
    """

    try:

        if g.user.verify_password(request.json[u"password"].encode("utf-8")) and request.json[u"new_password"] == request.json[u"confirm_password"]:
            g.user.hash_password(request.json[u"new_password"].encode("utf-8"))

        g.user.save()

        return jsonify(update_password=True)

    except Exception, ex:
        log.error(ex.message)
        abort(500)

@app.route('/api/update_user_infos', methods=['POST'])
@auto.doc()
@auth.login_required
def update_user_infos():
    """
    Check Status IMEI.
    Ex.:
    {
        "email": "fulano@gmail.com",
        "name": "Fulano",
        "surname": "de Tal",
        "gender": "masculino",
        "birth_date": "03-10-1993"
    }
    """

    if not request.json:
        log.error("JSON invalido")
        abort(400)

    user_id = uuid.uuid4()
    email = request.json[u'email'].encode('utf-8')
    name = request.json[u'name'].encode('utf-8')
    surname = request.json[u'surname'].encode('utf-8')
    gender = request.json[u'gender'].encode('utf-8')
    birth_date = None

    try:
        birth_date = datetime.strptime(request.json[u'birth_date'].encode('utf-8'), "%d-%m-%Y")
    except ValueError, ex:
        abort(400)

    try:
        if email is None or name is None or surname is None or gender is None or birth_date is None:
            abort(400)  # missing arguments

        if User.objects.filter(email=email).first():
            abort(400)  # existing email

        g.user.user_id = user_id
        g.user.email = email
        g.user.name = name
        g.user.surname = surname
        g.user.gender = gender
        g.user.birth_date = birth_date

        g.user.save()

        return jsonify(username=g.user.email, updated=True)
    except Exception, ex:
        log.error(str(ex))
        abort(500)


@app.route('/api/check_imei', methods=['POST'])
@auto.doc()
def check_imei():
    """
    Check Status IMEI.
    Ex.:
    {
        "IMEI": "1234567890123"
    }

    curl -H "Content-Type: application/json" -X POST -d '{ "IMEI": "123465"}' http://localhost:80/api/check_imei
    """

    try:
        device = Device.objects.filter(imei=request.json[u'IMEI'].encode('utf-8')).first()
        if device:
            device_status = Device_Status.objects.filter(device_id=device.device_id).order_by('-data_insercao').first()
            if device_status is not None:
                return jsonify(imei=request.json[u'IMEI'].encode('utf-8'), status=device_status.status, infos=device_status.info_status)

        return jsonify(imei=request.json[u'IMEI'].encode('utf-8'), status="IMEI não cadastrado")

    except Exception, ex:
        log.error(ex.message)
        abort(500)

@app.route('/api/get_devices', methods=['GET'])
@auto.doc()
@auth.login_required
def get_devices():
    """
    curl -H "Content-Type: application/json" -X POST -d '{ "IMEI": "123465"}' http://localhost:80/api/check_imei
    """

    try:
        result = []
        devices = Device.objects.filter(user_id=g.user.user_id)
        if devices:
            for device in devices:
                result.append(dict(device))
            return jsonify(devices=result)
        else:
            return jsonify(devices=None)
        
    except Exception, ex:
        log.error(ex.message)
        abort(500)

@app.route('/documentation')
def documentation():
    return auto.html()

if __name__ == '__main__':
    log.debug(args.debug)

    connection.setup(['127.0.0.1'], "checkimei")

    log.info("Starting API")

    app.run(host="0.0.0.0", port=80, debug=args.debug)
